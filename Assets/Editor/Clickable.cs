using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(CPU_MeshGenerator)), CanEditMultipleObjects]
public class Clickable : Editor
{
    CPU_MeshGenerator meshGenerator;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    protected virtual void OnSceneGUI()
    {
        meshGenerator = GameObject.Find("MeshGenerator").GetComponent<CPU_MeshGenerator>();
        if (meshGenerator == null)
        {
            return;
        }
        if (meshGenerator.showBoundsGizmo)
        {
            int numPointsPerAxis = meshGenerator.numPointsPerAxis;
            // for (int x = 0; x < numPointsPerAxis ; x++)
            // {
            //     for (int y = 0; y < numPointsPerAxis ; y++)
            //     {
            //         for (int z = 0; z < numPointsPerAxis ; z++)
            //         {
            //             int index = meshGenerator.indexFromCoord(x, y, z);
            //             Handles.Label(meshGenerator.points[index] * 1.2f, "" + index);

            //             if (meshGenerator.points[index].w > 0)
            //             {
            //                 Handles.color = Color.white;

            //             }
            //             else
            //             {
            //                 Handles.color = Color.black;

            //             }
            //             if (Handles.Button(meshGenerator.points[index], Quaternion.identity, meshGenerator.handleSphereSize, meshGenerator.handleSphereSize * 1.5f, Handles.SphereHandleCap))
            //             {
            //                 Debug.Log("The button " + index + " was pressed!");
            //                 meshGenerator.toggleCubeCorner(index);

            //             }
            //         }
            //     }
            // }
            for (int i = 0; i < meshGenerator.points.Length; i++)
            {

                Handles.Label(meshGenerator.points[i] * 1.2f, "" + i);

                if (meshGenerator.points[i].w > 0)
                {
                    Handles.color = Color.white;

                }
                else
                {
                    Handles.color = Color.black;

                }
                if (Handles.Button(meshGenerator.points[i], Quaternion.identity, meshGenerator.handleSphereSize, meshGenerator.handleSphereSize * 1.5f, Handles.SphereHandleCap))
                {
                    Debug.Log("The button " + i + " was pressed!");
                    meshGenerator.toggleCubeCorner(i);

                }

            }
            // for (int i = 0; i < meshGenerator.cubeCorners.Length; i++)
            // {
            //     Handles.Label(meshGenerator.cubeCorners[i] * 1.2f, "" + i);

            //     if (meshGenerator.cubeCorners[i].w > 0)
            //     {
            //         Handles.color = Color.black;
            //     }
            //     else
            //     {
            //         Handles.color = Color.white;
            //     }
            //     if (Handles.Button(meshGenerator.cubeCorners[i], Quaternion.identity, meshGenerator.handleSphereSize, meshGenerator.handleSphereSize * 2f, Handles.SphereHandleCap))
            //     {
            //         Debug.Log("The button " + i + " was pressed!");
            //         meshGenerator.toggleCubeCorner(i);

            //     }
            // }
        }
    }


}
