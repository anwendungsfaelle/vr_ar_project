using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

[ExecuteInEditMode]
public class MeshGenerator : MonoBehaviour
{

    struct Triangle
    {
#pragma warning disable 649 // disable unassigned variable warning
        public Vector3 a;
        public Vector3 b;
        public Vector3 c;

        public Vector3 this[int i]
        {
            get
            {
                switch (i)
                {
                    case 0:
                        return a;
                    case 1:
                        return b;
                    default:
                        return c;
                }
            }
        }
    }

    [Header("Test Marching Cubes Optimization")]
    public bool optimizeVertices = false;


    [Header("Mesh Settings")]
    // public Mesh mesh;
    public Material material;
    public Gradient gradient;





    [Header("General Settings")]
    public DensityGenerator densityGenerator;

    public ComputeShader shader;

    [Header("Voxel Settings")]
    public float isoLevel;
    public float boundsSize = 1;
    public Vector3 offset = Vector3.zero;

    [Range(2, 100)]
    public int numPointsPerAxis = 2;



    [Header("Gizmos")]
    public bool showBoundsGizmo = true;
    public Color boundsGizmoColor = Color.white;
    public float handleSphereSize = 0.05f;

    private bool settingsUpdated;

    ComputeBuffer pointsBuffer;
    ComputeBuffer triangleBuffer;
    ComputeBuffer triCountBuffer;

    private GameObject chunkHolder;
    public List<Chunk> chunkList;
    private string chunkHolderName = "Chunk Holder";

    public bool fixedMapSize = true;
    public Vector3Int numChunks = Vector3Int.one;

    void OnValidate()
    {

        settingsUpdated = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        // mesh = new Mesh();
        // if (GetComponent<MeshFilter>().mesh != null)
        // {
        //     DestroyImmediate(GetComponent<MeshFilter>().mesh);
        // }
        // GetComponent<MeshFilter>().mesh = mesh;
        GetComponent<MeshRenderer>().material = material;

    }

    // Update is called once per frame
    void Update()
    {

        if (settingsUpdated)
        {

            RequestMeshUpdate();
            settingsUpdated = false;



        }

    }

    public void RequestMeshUpdate()
    {
        CreateBuffers();
        if (fixedMapSize)
        {
            CreateFixedChunks();
            // print("CREATING CHUNKS AND UPDATING MESH");
            foreach (Chunk chunk in chunkList)
            {
                GeneratePoints(chunk);
                UpdateMesh(chunk);
            }
        }
        ReleaseBuffers();

        // CreateBuffers();
        // GeneratePoints();
        // UpdateMesh();
        // ReleaseBuffers();
    }

    void GeneratePoints(Chunk chunk)
    {
        float pointSpacing = boundsSize / (numPointsPerAxis - 1);

        Vector3Int coord = chunk.id;
        Vector3 centre = centreFromCoord(coord);
        Vector3 worldBounds = new Vector3(numChunks.x, numChunks.y, numChunks.z) * boundsSize;

        densityGenerator.Generate(pointsBuffer, numPointsPerAxis, boundsSize, worldBounds, centre, offset, pointSpacing);
        // Vector4[] points = new Vector4[numPointsPerAxis * numPointsPerAxis * numPointsPerAxis];
        // pointsBuffer.GetData(points);
        // for(int i = 0; i < points.Length; i++){
        //     print($"POINT {i}: {points[i]}");
        // }
    }

    void UpdateMesh(Chunk chunk)
    {
        int threadGroupSize = 8;
        int numVoxelsPerAxis = numPointsPerAxis - 1;
        int numThreadsPerAxis = Mathf.CeilToInt(numVoxelsPerAxis / (float)threadGroupSize);

        triangleBuffer.SetCounterValue(0);

        shader.SetBuffer(0, "points", pointsBuffer);
        shader.SetBuffer(0, "triangles", triangleBuffer);
        shader.SetInt("numPointsPerAxis", numPointsPerAxis);
        shader.SetFloat("isoLevel", isoLevel);
        shader.Dispatch(0, numThreadsPerAxis, numThreadsPerAxis, numThreadsPerAxis);

        // Get number of triangles in the triangle buffer
        ComputeBuffer.CopyCount(triangleBuffer, triCountBuffer, 0);
        int[] triCountArray = { 0 };
        triCountBuffer.GetData(triCountArray);
        int numTris = triCountArray[0];

        // Get triangle data from shader
        Triangle[] tris = new Triangle[numTris];
        triangleBuffer.GetData(tris, 0, 0, numTris);

        Mesh mesh = chunk.mesh;

        Vector3[] vertices = new Vector3[numTris * 3];
        int[] trianglesArray = new int[numTris * 3];
        Color[] colors = new Color[vertices.Length];

        float boundsY = boundsSize * numChunks.y;


        for (int i = 0; i < numTris; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                int index = i * 3 + j;
                trianglesArray[index] = index;
                vertices[index] = tris[i][j];
                float height = Mathf.InverseLerp(-boundsY / 2, boundsY / 2, vertices[index].y); //TODO Normals?
                colors[index] = gradient.Evaluate(height);
            }
        }
        
        
        if (optimizeVertices)
        {
            var vertexIndexMap = new Dictionary<Vector3, int>();
            List<Vector3> vertexList = new List<Vector3>();
            List<int> processedTriangles = new List<int>();
            List<Color> colorList = new List<Color>();
            int triangleIndex = 0;
            for (int k = 0; k < vertices.Length; k++)
            {
                Vector3 vertex = vertices[k];
                int sharedVertexIndex;
                if (vertexIndexMap.TryGetValue(vertex, out sharedVertexIndex))
                {
                    processedTriangles.Add(sharedVertexIndex);
                }
                else
                {
                    vertexList.Add(vertex);
                    colorList.Add(colors[k]);
                    processedTriangles.Add(triangleIndex);
                    vertexIndexMap.Add(vertex, triangleIndex);
                    triangleIndex++;
                }
            }

            vertices = vertexList.ToArray();
            trianglesArray = processedTriangles.ToArray();
            colors = colorList.ToArray();

        }

        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = trianglesArray;
        mesh.colors = colors;
        mesh.RecalculateNormals();



    }



    void CreateBuffers()
    {
        int numPoints = numPointsPerAxis * numPointsPerAxis * numPointsPerAxis;
        int numVoxelsPerAxis = numPointsPerAxis - 1;
        int numVoxels = numVoxelsPerAxis * numVoxelsPerAxis * numVoxelsPerAxis;
        int numMaxTriangles = numVoxels * 5;

        // if (pointsBuffer == null || numPoints != pointsBuffer.count)
        // {
        triangleBuffer = new ComputeBuffer(numMaxTriangles, sizeof(float) * 3 * 3, ComputeBufferType.Append);
        pointsBuffer = new ComputeBuffer(numPoints, sizeof(float) * 4);
        triCountBuffer = new ComputeBuffer(1, sizeof(int), ComputeBufferType.Raw);
        // }
    }

    void ReleaseBuffers()
    {
        if (triangleBuffer != null)
        {
            pointsBuffer.Release();
            triangleBuffer.Release();
            triCountBuffer.Release();
        }
    }



    void OnDrawGizmos()
    {
        if (showBoundsGizmo)
        {
            Gizmos.color = boundsGizmoColor;
            float chunkBoundsPadding = 1f;

            // Vector3Int currentChunk = Vector3Int.zero; //TODO
            foreach (Chunk chunk in chunkList)
            {
                Gizmos.color = Color.grey;
                Gizmos.DrawWireCube(centreFromCoord(chunk.id), boundsSize * Vector3.one * chunkBoundsPadding);

            }

        }
    }
    public Vector3 centreFromCoord(Vector3Int coord)
    {
        Vector3Int numChunks = new Vector3Int(1, 1, 1);
        Vector3 totalBounds = (Vector3)numChunks * boundsSize;
        return -totalBounds / 2 + (Vector3)coord * boundsSize + Vector3.one * boundsSize / 2;
    }


    void CreateFixedChunks()
    {
        chunkList = new List<Chunk>();
        List<Chunk> oldChunks = new List<Chunk>(FindObjectsOfType<Chunk>());

        for (int x = 0; x < numChunks.x; x++)
        {
            for (int y = 0; y < numChunks.y; y++)
            {
                for (int z = 0; z < numChunks.z; z++)
                {
                    Vector3Int coord = new Vector3Int(x, y, z);
                    bool chunkAlreadyExists = false;

                    // If chunk already exists, add it to the chunks list, and remove from the old list.
                    for (int i = 0; i < oldChunks.Count; i++)
                    {
                        if (oldChunks[i].id == coord)
                        {
                            chunkList.Add(oldChunks[i]);
                            oldChunks.RemoveAt(i);
                            chunkAlreadyExists = true;
                            break;
                        }
                    }

                    // Create new chunk
                    if (!chunkAlreadyExists)
                    {
                        var newChunk = CreateChunk(coord);
                        chunkList.Add(newChunk);
                    }
                }
            }
        }
        List<Chunk> chunksToDestroy = oldChunks.Where(oldChunk => chunkList.Any(chunk => chunk.id != oldChunk.id)).ToList();
        chunksToDestroy.ForEach(chunk => chunk.DestroyOrDisable());
    }


    Chunk CreateChunk(Vector3Int coord)
    {
        string chunkName = $"Chunk {coord.x},{coord.y},{coord.z}";
        GameObject chunkGO = new GameObject(chunkName);
        if (chunkHolder == null)
        {
            if (GameObject.Find(chunkHolderName))
            {
                chunkHolder = GameObject.Find(chunkHolderName);
            }
            else
            {
                chunkHolder = new GameObject(chunkHolderName);
            }
        }
        chunkGO.transform.parent = chunkHolder.transform;
        Chunk chunk = chunkGO.AddComponent<Chunk>();
        chunk.setUp(coord, material);
        return chunk;
    }

}
