#pragma kernel Density
#include "/Includes/Density.compute"
#include "/Includes/Noise.compute"

// Noise settings
bool useFBM;
StructuredBuffer<float3> offsets;
int octaves;
float lacunarity;
float persistence;
float noiseScale;
float noiseWeight;
float floorOffset;
float weightMultiplier;
bool closeEdges;
float hardFloor;
float hardFloorWeight;

float4 params;

float fBM (float3 pos){
    float noiseValue = 0;
    float frequency = noiseScale / 100;
    float amplitude = 1;
    float weight = 1;
    //loop of octaves
    for(int i = 0; i < octaves; i++){
        float n = 1-abs(snoise(pos*frequency+offsets[i] + offset));
        n *= n;
        n *= weight;
        weight = max(min(n*weightMultiplier,1),0);

        noiseValue += n*amplitude;

        amplitude *= persistence;
        frequency *= lacunarity;
    }
    return noiseValue;
}

float fBM2(float3 pos){
     // Initial values
    float noiseValue = 0.0;
    float amplitude = 1;
    float frequency = noiseScale/100;
    // float frequency = 0.5;

    for (int i = 0; i < octaves; i++) {
        float n = snoise(pos * frequency);
        noiseValue += amplitude *n;

        amplitude *= persistence;
        frequency *= lacunarity;
    }
    return noiseValue;
}

[numthreads(numThreads,numThreads,numThreads)]
void Density (int3 id : SV_DispatchThreadID)
{
    if (id.x >= numPointsPerAxis || id.y >= numPointsPerAxis || id.z >= numPointsPerAxis) {
        return;
    }

    float3 pos = centre + id * spacing - boundsSize/2;
    float noise = 0.0f;
    float finalVal = 0.0f;
    if(useFBM){
         noise = fBM(pos);
         finalVal = -(pos.y + floorOffset) + noise * noiseWeight;
        

        if (pos.y < hardFloor) {
            finalVal += hardFloorWeight;
        }

    } else {
        // noise = snoise(pos);
        noise = fBM2(pos);
         finalVal = -(pos.y + floorOffset) + noise * noiseWeight;

    }


    // if (closeEdges) {
    //     float3 edgeOffset = abs(pos*2)-worldSize + spacing/2;
    //     float edgeWeight = saturate(sign(max(max(edgeOffset.x,edgeOffset.y),edgeOffset.z)));
    //     finalVal = finalVal * (1-edgeWeight) - 100 * edgeWeight;
        
    // }

    int index = indexFromCoord(id.x,id.y,id.z);
    points[index] = float4(pos, finalVal);
    
}
